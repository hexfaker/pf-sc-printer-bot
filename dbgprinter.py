import logging

import printer


class Printer(printer.Printer):
    """Debug printer. Prints file name to log"""
    def print(self, file: str) -> bool:
        logging.info(msg="Kinda printing " + file)
        return True
