import logging as logging
import os

from easyimap import easyimap
from typing import List, Tuple
from email.header import decode_header

from printer import Printer

from configuration import config


class _File:
    """Class keeps file name and content and can print itself to printer"""

    def __init__(self, name: str, content: bytes):
        self._name = name
        self._content = content

    def print(self, printer: Printer) -> bool:
        path = config["files"]["path"] + self._name
        logging.debug("Saving {} as {}".format(self._name, path))
        try:
            with open(path, "wb") as f:
                f.write(self._content)
            return printer.print(path)
        except Exception as e:
            logging.warning("Unexpected error: {}".format(str(e)))
        finally:
            logging.debug("Removing " + path)
            os.remove(path)
        return False

    def __str__(self):
        return self._name

    def __repr__(self):
        return str(self)

    def name(self):
        return self._name


class _Task:
    """Represent printing task with its requester, text, and files"""

    def __init__(self, requester: str, title: str, body: str, files: List[_File]):
        self.title = title
        self.body = body
        self.requester = requester
        self.files = files
        pass

    def do(self, printer: Printer) -> List[Tuple[str, bool]]:
        """Tries to print all files in task"""
        return [(file.name(), file.print(printer)) for file in self.files]

    def __str__(self):
        return "Task[title={},body={},requester={},files={}]".format(self.title, self.body, self.requester, self.files)

    def __repr__(self):
        return str(self)


class TaskSource:
    def __init__(self, cfg: dict):
        self._imap = easyimap.connect(host=cfg["host"], port=cfg.get("port", 993),
                                      user=cfg["login"], password=cfg["password"],
                                      read_only=cfg.get("read_only", False))

    def next(self) -> _Task:
        # Retrieve one unread message
        mail = self._imap.unseen(1)
        if len(mail) != 0:
            mail = mail[0]
            # Parse body of  message
            text = str()
            # noinspection PyBroadException
            try:
                text = mail.body
            except Exception:
                pass

            return _Task(mail.return_path, mail.title, text,
                         [_File(decode_header(x[0])[0][0].decode(), x[1]) for x in mail.attachments])
        return None


class TaskSupervisor:
    def __init__(self, source: TaskSource, printer: Printer):
        self.source = source
        self.printer = printer

    def do(self):
        # noinspection PyBroadException
        try:
            logging.debug("Begin printing session")
            t = self.source.next()

            while not (t is None):
                res = t.do(self.printer)
                logging.debug("Printed {} with result {}".format(t, res))
                t = self.source.next()

            logging.debug("Printing session completed")
        except Exception as e:
            logging.warning("Printing session failed: " + str(e))
