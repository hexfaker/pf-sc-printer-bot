import win32api
import printer
import win32event
import logging
import win32com
import win32com.shell.shell as shell


class Printer(printer.Printer):
    def print(self, file: str) -> bool:
        try:
            logging.debug("Trying to print " + file)
            shell.ShellExecuteEx(fMask =  0x100, lpVerb='print', lpFile=file) # 0x00000100 - SEE_MASK_NOASYNC flag (sync execution) 
            return True
        except Exception as e:
            logging.warning("Printing failed" + str(e))
            return False
