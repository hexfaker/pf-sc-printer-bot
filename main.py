import importlib
import logging
from configuration import config


def main():
    from time import sleep
    from tasks import TaskSource, TaskSupervisor

    logging.info("Starting...")

    # Dynamically load printer implementation
    printer_m = importlib.import_module(config["printer"], ".")
    printer_class = getattr(printer_m, "Printer")
    logging.info("Printing class: " + str(printer_class))

    source = TaskSource(config["imap"])
    supervisor = TaskSupervisor(source, printer_class())
    try:
        while True:
            supervisor.do()
            sleep(config.get("pause", 30))
    finally:
        logging.info("Exiting...")


if __name__ == '__main__':
    main()
