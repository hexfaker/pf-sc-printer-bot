
class Printer:
    def print(self, file: str) -> bool:
        raise NotImplemented()
