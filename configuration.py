import yaml
import logging
import sys


config = yaml.load(open("config.yml"))


def configure_logging():
    logging.basicConfig(format='%(asctime)s [%(module)s:%(name)s:%(lineno)d] %(levelname)s: %(message)s',
                        level=logging.DEBUG, stream=sys.stdout)

configure_logging()
